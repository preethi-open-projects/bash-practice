#!/bin/bash

number=$1
num_of_multiples=$2
total_args=$#
script_name=$0

############################################## Function starts here
func_usage() {
	echo
	echo "Usage1: $script_name <argument1> <argument2>"
	echo "Usage2: $script_name <argument1>"
	echo
}

func_check_valid_arg() {
   
	if [ $total_args -gt 2 ] ; then
		echo "Invalid arguments passed"
        fi
}

func_is_a_number() {
	if [ "$1" -eq "$1" ] 2>/dev/null; then
		return 0
	else
		return 1
	fi
}

################################################ Script starts here
func_usage
func_check_valid_arg

func_is_a_number $number
result=$?
if ! [ $result -eq 0 ] ; then
	echo "Invalid number"
	exit 1
fi

func_is_a_number $num_of_multiples
result=$?
if ! [ $result -eq 0 ] ; then
	echo "Invalid number"
	exit 1
fi

if [ $total_args -eq 1 ] ; then
	echo -n "The multiples of $1: "
	for num in {1..10}
	do
		result=$((number*num))
		echo -n " $result"
	done
	echo

elif [ $total_args -eq 2 ] ; then
        echo -n "The multiples of $1: "
        for num in $(seq $num_of_multiples)
        do
                result=$((number*num))
                echo -n " $result"
        done
        echo
fi





