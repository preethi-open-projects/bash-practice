#!/bin/bash

N1=$1
N2=$2
N3=$3
total_args=$#
script_name=$0

################################################Function starts here
func_usage() {
	echo
	echo "Usage: $script_name <number1> <number2> <number3>"
	echo
}

func_check_validity() {
	if ! [ $total_args -eq 3 ] ; then
		echo "Invalid number of arguments"
		exit 1
	fi

}

func_is_a_number() {	
	if  [ "$1" -eq "$1" ] 2>/dev/null; then
		return 0
	else
		return 1
	fi
	

}
################################################Script starts here

func_usage
func_check_validity

func_is_a_number $N1
result=$?

if ! [ $result -eq 0 ] ; then
	echo "Invalid number"
	exit 1
fi

func_is_a_number $N2
result=$?

if ! [ $result -eq 0 ] ; then
	echo "Invalid number"
	exit 1
fi

func_is_a_number $N3
result=$?

if ! [ $result -eq 0 ] ; then
	echo "INvalid number"
	exit 1
fi




Result=$((N1+N2+N3))


echo "N1: $1"
echo "N2: $2"
echo "N3: $3"
echo "$N1+$N2+$N3=$Result"
